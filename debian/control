Source: moc
Section: sound
Priority: optional
Maintainer: Elimar Riesebieter <riesebie@lxtec.de>
Standards-Version: 4.6.1
Build-Depends: chrpath,
               debhelper-compat (= 13),
               libasound2-dev [linux-any],
               libaspell-dev,
               libavcodec-dev,
               libavformat-dev,
               libcurl4-gnutls-dev,
               libdb-dev,
               libfaad-dev,
               libflac-dev,
               libid3tag0-dev,
               libjack-jackd2-dev,
               libltdl-dev,
               libmad0-dev,
               libmagic-dev,
               libmodplug-dev,
               libmpcdec-dev,
               libncurses5-dev,
               libncursesw5-dev,
               libogg-dev,
               libopusfile-dev,
               libpopt-dev,
               librcc-dev,
               libresid-builder-dev,
               libsamplerate0-dev,
               libsidplay2-dev,
               libsidutils-dev,
               libsndfile1-dev,
               libspeex-dev,
               libtagc0-dev,
               libtool,
               libvorbis-dev,
               libwavpack-dev,
               libxml2-dev,
               zlib1g-dev
Homepage: https://moc.daper.net
Vcs-Browser: https://salsa.debian.org/riesebie/moc
Vcs-Git: https://salsa.debian.org/riesebie/moc.git
Rules-Requires-Root: no

Package: moc
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: moc-ffmpeg-plugin
Description: ncurses based console audio player
 moc (music on console) is a full-screen player designed to be powerful
 and easy to use.
 .
 Supported file formats are: MP3, OGG Vorbis, FLAC, OPUS, WAVE, SPEEX, Musepack
 (MPC), AIFF, AU (and other less popular formats supported by libsndfile).
 New formats support is under development.
 .
 Other features: simple mixer, colour themes, searching the menu (the playlist
 or a directory) like M-s in Midnight Commander, the way MOC creates titles
 from tags is configurable, optional character set conversion for file tags
 using iconv(), OSS or ALSA output.

Package: moc-ffmpeg-plugin
Architecture: any
Multi-Arch: same
Depends: moc (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: ncurses based console audio player - ffmpeg plugin
 moc (music on console) is a full-screen player designed to be powerful
 and easy to use.
 .
 moc-ffmpeg-plugin is an additional plugin to play soundfiles out of the ffmpeg
 libs like WMA, RealAudio, MP4 and AAC.
